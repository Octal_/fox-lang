- Compiled but with comptime interpretation
- Native JVM/Rust/C/CPython interop ( aka `export (java|rust|c|python) ({}|DEF)` )
- Pointers are refcounted
- Not for embedded
- Ability to manually use ASM in blocks ( `asm {}` )
- Inline functions
- Comments ( `/* block comment */` `// line comment` )
- Lambda as type ( `() -> void` is callable with 0 params and that returns void )


Example Hello world
```cpp
import print from std.console

export "c" main( argv: String[] ) -> void {
    print( 'Hello World!' )
}
```

Example structs
```cpp
export "c" owo() -> 0 // direct return, similar to 'fun owo() = 0' in Kotlin

struct uwu // Defines an empty struct

export "c" uwu::nya() -> void { // Implicitly has a first parameter of type 'uwu'
  //...
}
```
